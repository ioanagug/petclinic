package com.petclinic.enums;

public enum Type {
    DOG, CAT, FISH, PARROT
}
