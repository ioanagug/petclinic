package com.petclinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Consult {
    @Id
    @GeneratedValue
    private Integer consultId;

    private String description;

    @OneToOne
    @JoinColumn
    private Appointment appointment;
}
