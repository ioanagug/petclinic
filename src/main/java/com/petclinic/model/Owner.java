package com.petclinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class Owner {
    @Id
    @GeneratedValue
    private Integer ownerId;

    private String firstName;
    private String LastName;
    private String phoneNo;

    @OneToMany (mappedBy = "owner")
    private List<Pet> pets;
}
