package com.petclinic.model;

import com.petclinic.enums.Type;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Vet {
    @Id
    @GeneratedValue
    private Integer vetId;

    private String firstName;
    private String lastName;
    private String address;

    @ElementCollection(targetClass = Type.class)
    @CollectionTable(name = "vet_type",
            joinColumns = @JoinColumn(name = "vetId"))
    private List<Type> specialities;

    @OneToMany (mappedBy = "vet")
    private List<Appointment> appointments;
}
